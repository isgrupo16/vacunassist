Rails.application.routes.draw do
  get '/enfermeros/listado/:id', to: "enfermeros#baja", as: "baja"
  get '/pacientes/:id', to: "main#alta", as: "main_alta"
  get 'estadisticas/mostrar'
 
 ## get 'estadisticas/seleccionar', to: "estadisticas#seleccionar", as: "seleccionar"
  get 'enfermeros/listado', to: "enfermeros#listado", as: "listadoenfermero"
  patch 'turno/:id', to: "turnos#turno", as: "turno"  ## para guardar la nota
  patch '/aplicadasinturnoform', to: "comprobante#aplicadasinturnoform", as: "sinturno" 
  patch '/crearturnofiebreamarilla/:id', to: "turnos#crearturnofiebreamarilla", as: "crearturnofiebreamarilla"
  patch '/crearturnogripe/:id', to: "turnos#crearturnogripe", as: "crearturnogripe"
  patch '/crearturnocovid2/:id', to: "turnos#crearturnocovid2", as: "crearturnocovid2"
  patch '/crearturnocovid1/:id', to: "turnos#crearturnocovid1", as: "crearturnocovid1"
  get 'comprobante/index/:id', to: "comprobante#index", as: "comprobante_index"
  get '/crearcomprobante/:id', to: "comprobante#crearcomprobante", as: "crearcomprobante"
  get '/historialdeturnos', to: "turnos#historialdeturnos", as: "historialdeturnos"
   get '/historialdeturnospaciente/:id', to: "turnos#historialdeturnospaciente", as: "historialdeturnospaciente"
   get '/aplicadasinturno', to: "turnos#aplicadasinturno", as: "aplicadasinturno"
  get '/noaplicada/:id', to: "turnos#noaplicada", as: "noaplicada"
  get '/noasistencia/:id', to: "turnos#noasistencia", as: "noasistencia"
  get '/asistencia/:id', to: "turnos#asistencia", as: "asistencia"
  get 'turnos/listado', to: "turnos#listado", as: "ver_lista_turnos"
  get 'vacunatorios/ver_contacto'
  devise_for :users, controllers:{registrations: 'users/registrations' }
  get "/verdatos", to: "main#ver", as:"verdatos"
  get "/verlistadoturnos", to: "main#verlistadoturnos", as:"verlistadoturnos"
  get "/verdetalleturno/:id", to: "main#verdetalleturno", as:"verdetalleturno"
  get "/verhistoria/:id", to: "main#verhistoria", as:"verhistoria"
  get "/covid1", to: "main#turnoscovid1", as:"turnoscovid1"
  get "/covid2", to: "main#turnoscovid2", as:"turnoscovid2"
  get "/fiebreamarilla", to: "main#turnosfiebreamarilla", as:"turnosfiebreamarilla"
  get "/gripe", to: "main#turnosgripe", as:"turnosgripe"
  get "/pacientes", to: "main#pacientes", as:"pacientes"
  get "/obtenerturno/:id" , to: "main#obtenerturno", as:"obtenerturno"
  get "/cancelarturno/:id" , to: "main#cancelarturno", as:"cancelarturno"
  get "/cancelarturnocovid1/:id" , to: "main#cancelarturnocovid1", as:"cancelarturnocovid1"
  get "/cancelarturnocovid2/:id" , to: "main#cancelarturnocovid2", as:"cancelarturnocovid2"
  get "/buscar", to: "main#search", as:"buscar"
  get "/buscarturnos", to: "turnos#searchturnos", as:"searchturnos"
  get "/buscarenfermero", to: "main#searchenfermero", as:"buscarenfermero"
  get "/verdatospaciente/:id", to: "main#verdatospaciente", as:"verdatospaciente"
  get "/verinformacionenfermero/:id", to: "enfermeros#verinformacionenfermero", as:"verinformacionenfermero"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
   get "/enfermero", to: "main#pantallaenfermero", as:"pantallaenfermero"
   get "/admin", to: "main#pantallaadmin", as:"pantallaadmin"
  get "/home", to: "main#home", as:"home"
  root 'main#home'
end
