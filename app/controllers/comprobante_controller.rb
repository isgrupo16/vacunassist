class ComprobanteController < ApplicationController
  def index
    @c = Comprobante.where(user_id: params[:id])
    @usuario = User.find(params[:id])
  end
  def crearcomprobante
    redirect_to ver_lista_turnos_path(current_user), notice: "Se creó el certificado"
    @turno = Turno.find(params[:id])
    @user = User.find(@turno.user_id)
    @vacunatorio = Vacunatorio.find(@turno.vacunatorio_id)
    Comprobante.create(vacuna: @turno.nombrevacuna, fecha: @turno.fecha, hora: @turno.hora, user_id: @user.id, vacunatorio_id: @vacunatorio.id)
    @turno.aplicada = true
    @turno.asistencia = true
    @turno.save
    if @turno.nombrevacuna == "covid1"
        @user.covid1 = true
    elsif @turno.nombrevacuna == "covid2"
        @user.covid2 = true
    elsif @turno.nombrevacuna == "fiebreamarilla"
        @user.fiebreamarilla = true
    elsif @turno.nombrevacuna == "gripe"
       @user.gripe = true
    end
    @user.save
    UserMailer.with(usuario: @user).nuevo_certi.deliver_later
  end
  
  
  
  def aplicadasinturnoform
    @user = User.find_by(dni: params[:dni])
    @covid1 = params[:covid1]
    @covid2 = params[:covid2]
    @fiebreamarilla = params[:fiebreamarilla]
    @gripe = params[:gripe]
    if @user != nil
         if (@covid1 == "1" and @user.covid1 == false and @covid2 == "0" and @fiebreamarilla == "0" and @gripe == "0")
              Comprobante.create(vacuna: "covid1", fecha: Date.today, hora: Time.now-10800, user_id: @user.id, vacunatorio_id: params[:vacunatorio])
             @user.covid1 = true
             @user.save
             UserMailer.with(usuario: @user).nuevo_certi.deliver_later
             @turno = Turno.where("user_id = ? and nombrevacuna = ? and fecha >= ?", @user.id, "covid1", Date.today).first
             if @turno != nil
                @turno.delete
             end
             redirect_to aplicadasinturno_path(current_user), notice: "Se creó el certificado"
         elsif (@covid1 == "0" and @user.covid2 == false and @covid2 == "1" and @fiebreamarilla == "0" and @gripe == "0")
              Comprobante.create(vacuna: "covid2", fecha: Date.today, hora: Time.now-10800, user_id: @user.id, vacunatorio_id: params[:vacunatorio])
              @user.covid2 = true
              @user.save
              UserMailer.with(usuario: @user).nuevo_certi.deliver_later
             @turno = Turno.where("user_id = ? and nombrevacuna = ? and fecha >= ?", @user.id, "covid2", Date.today).first
             if @turno != nil
                @turno.delete
             end
              redirect_to aplicadasinturno_path(current_user), notice: "Se creó el certificado"
        elsif (@covid1 == "0" and @covid2 == "0" and @user.fiebreamarilla == false and @fiebreamarilla == "1" and @gripe == "0")
              Comprobante.create(vacuna: "fiebreamarilla", fecha: Date.today, hora: Time.now-10800, user_id: @user.id, vacunatorio_id: params[:vacunatorio])
              @user.fiebreamarilla = true
              @user.save
              UserMailer.with(usuario: @user).nuevo_certi.deliver_later
             @turno = Turno.where("user_id = ? and nombrevacuna = ? and fecha >= ?", @user.id, "fiebreamarilla", Date.today).first
             if @turno != nil
                @turno.delete
             end
              redirect_to aplicadasinturno_path(current_user), notice: "Se creó el certificado"
        elsif (@covid1 == "0" and @covid2 == "0" and @fiebreamarilla == "0" and @gripe == "1" and @user.gripe == false)
              Comprobante.create(vacuna: "gripe", fecha: Date.today, hora: Time.now-10800, user_id: @user.id, vacunatorio_id: params[:vacunatorio])
               
              @user.gripe = true
              @user.save
              UserMailer.with(usuario: @user).nuevo_certi.deliver_later
             @turno = Turno.where("user_id = ? and nombrevacuna = ? and fecha >= ?", @user.id, "gripe", Date.today).first
             if @turno != nil
                @turno.delete
             end             
              redirect_to aplicadasinturno_path(current_user), notice: "Se creó el certificado"
        else 
            redirect_to aplicadasinturno_path(current_user), notice: "Debe seleccionar solo una vacuna que el paciente no tenga aplicada"
        end 
    else
      redirect_to aplicadasinturno_path(current_user), notice: "No hay usuario que coincida con el DNI ingresado"
    end
   
  end


end
