class ApplicationController < ActionController::Base
 before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?
  protected
     def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :dni, :apellido, :nacimiento, :direccion, :covid1, :covid2, :fiebreamarilla, :gripe])
        devise_parameter_sanitizer.permit(:account_update, keys: [:name, :dni, :apellido, :nacimiento, :direccion, :covid1, :covid2, :fiebreamarilla, :gripe, :historia])
         devise_parameter_sanitizer.permit(:sign_in, keys: [:email, :password])
     end
end
