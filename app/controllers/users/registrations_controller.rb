#frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
 ## before_action :configure_sign_up_params, only: [:create]
  ##before_action :configure_account_update_params, only: [:update]

## GET /resource/sign_up
  def new
    super
   end

##  POST /resource
  def create
  	super
  	if user_signed_in?
  		@user = current_user
  	

       	@edad = Date.today.year - @user.nacimiento.year 
       	if Date.today.month <= @user.nacimiento.month 
           		if Date.today.day <= @user.nacimiento.day
                		@edad = @edad-1
           		end 
        	end 
      		if not  @user.covid1
      			flash[:notice] = "¡Bienvenido! Se ha registrado correctamente.Se le ha asignado un turno para la primera dosis de COVID19. Lo puede ver en Ver Listado de Turnos"
			Turno.create(nombrevacuna: "covid1", user_id: @user.id, fecha: Date.today+5, hora: "10:00", vacunatorio_id: "1", asistencia: false, aplicada: false)
        	end
        	if not  @user.covid2 and @user.covid1
      			flash[:notice] = "¡Bienvenido! Se ha registrado correctamente. Se le ha asignado un turno para la segunda dosis de COVID19. Lo puede ver en Ver Listado de Turnos"
			Turno.create(nombrevacuna: "covid2", user_id: @user.id, fecha: Date.today+5, hora: "10:00", vacunatorio_id: "1", asistencia: false, aplicada: false)
        	end
      		if @edad>60 and @user.gripe == false
      		flash[:notice] = "¡Bienvenido! Se ha registrado correctamente. Se ha asignado un turno para la vacuna contra la gripe. Lo puede ver en Ver Listado de Turnos"
        		Turno.create(nombrevacuna: "gripe", user_id: @user.id, fecha: Date.today+5, hora: "10:00", vacunatorio_id: "1", asistencia: false, aplicada: false)
        	end
      	end
      	@user.rol = "1"
      	@user.save

  end

 ## GET /resource/edit
  def edit
     super




   end

 ##  PUT /resource
   def update
     super



   end

  ## DELETE /resource
   def destroy
     if Turno.all.where(user_id: current_user.id).exists?
       Turno.all.where(user_id: current_user.id).each do |turno|
          turno.delete
       end
    end
    if Comprobante.all.where(user_id: current_user.id).exists?
       Comprobante.all.where(user_id: current_user.id).each do |c|
          c.delete
       end
     end
     super
  end

  ## GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
   def cancel
     super
   end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
