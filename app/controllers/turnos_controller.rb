class TurnosController < ApplicationController
    def listado
       ## @t = Turno.all.where(["fecha = ? and user_id != ?", Date.today, "nil"])
        @t = Turno.all.where(fecha: Date.today).where.not(user_id: nil)
       
    end
    def noaplicada
    redirect_to ver_lista_turnos_path(current_user), notice: "Se marcó vacuna no aplicada"
        @turno = Turno.find(params[:id])
        @turno.aplicada = false
        @turno.save
    end
    def asistencia
    redirect_to ver_lista_turnos_path(current_user), notice: "Se marcó asistencia al turno"
        @turno = Turno.find(params[:id])
        @turno.asistencia = true
        @turno.save
    end
    def noasistencia
    redirect_to ver_lista_turnos_path(current_user), notice: "Se marcó no asistencia al turno"
    	 @turno = Turno.find(params[:id])
    	 @turno.asistencia = false
    	 @turno.save
    end 
    def turno ## para la nota
      redirect_to ver_lista_turnos_path(current_user), notice: "Se agregó la nota con éxito"
      @t = Turno.find(params[:id])
      @texto = params[:turno][:texto]
      @t.texto = @texto
      @t.save
    
    end
    def crearturnocovid1
      redirect_to home_path(current_user), notice: "Se obtuvo el turno correctamente"
      @fecha = params[:fecha]
      @id = current_user.id
      @t = Turno.create(nombrevacuna: "covid1", fecha: params[:fecha], hora: params[:hora], user_id: current_user.id, vacunatorio_id: params[:vacunatorio])
      @dias = (Date.today...@t.fecha).count
      if @dias <= 4
        UserMailer.with(usuario: current_user, fecha: @t.fecha).turno_prox.deliver_later
      else
        UserMailer.with(usuario: current_user, fecha: @t.fecha).turno_prox.deliver_later(wait_until: (@dias).days.from_now)
      end
    end
    def crearturnocovid1
      redirect_to home_path(current_user), notice: "Se obtuvo el turno correctamente"
      @fecha = params[:fecha]
      @id = current_user.id
      @t = Turno.create(nombrevacuna: "covid1", fecha: params[:fecha], hora: params[:hora], user_id: current_user.id, vacunatorio_id: params[:vacunatorio], asistencia: false, aplicada: false)
      @dias = (Date.today...@t.fecha).count
      if @dias <= 4
        UserMailer.with(usuario: current_user, fecha: @t.fecha).turno_prox.deliver_later
      else
        UserMailer.with(usuario: current_user, fecha: @t.fecha).turno_prox.deliver_later(wait_until: (@dias).days.from_now)
      end
    end
    def crearturnocovid2
      redirect_to home_path(current_user), notice: "Se obtuvo el turno correctamente"
      @fecha = params[:fecha]
      @id = current_user.id
      @t = Turno.create(nombrevacuna: "covid2", fecha: params[:fecha], hora: params[:hora], user_id: current_user.id, vacunatorio_id: params[:vacunatorio], asistencia: false, aplicada: false)
      @dias = (Date.today...@t.fecha).count
      if @dias <= 4
        UserMailer.with(usuario: current_user, fecha: @t.fecha).turno_prox.deliver_later
      else
        UserMailer.with(usuario: current_user, fecha: @t.fecha).turno_prox.deliver_later(wait_until: (@dias).days.from_now)
      end
    end
    def crearturnogripe
      redirect_to home_path(current_user), notice: "Se obtuvo el turno correctamente"
      @fecha = params[:fecha]
      @id = current_user.id
      @t = Turno.create(nombrevacuna: "gripe", fecha: params[:fecha], hora: params[:hora], user_id: current_user.id, vacunatorio_id: params[:vacunatorio], asistencia: false, aplicada: false)
      @dias = (Date.today...@t.fecha).count
      if @dias <= 4
        UserMailer.with(usuario: current_user, fecha: @t.fecha).turno_prox.deliver_later
      else
        UserMailer.with(usuario: current_user, fecha: @t.fecha).turno_prox.deliver_later(wait_until: (@dias).days.from_now)
      end
    end
    def crearturnofiebreamarilla
      redirect_to home_path(current_user), notice: "Se obtuvo el turno correctamente"
      @fecha = params[:fecha]
      @id = current_user.id
      @t = Turno.create(nombrevacuna: "fiebreamarilla", fecha: params[:fecha], hora: params[:hora], user_id: current_user.id, vacunatorio_id: params[:vacunatorio], asistencia: false, aplicada: false)
      @dias = (Date.today...@t.fecha).count
      if @dias <= 4
        UserMailer.with(usuario: current_user, fecha: @t.fecha).turno_prox.deliver_later
      else
        UserMailer.with(usuario: current_user, fecha: @t.fecha).turno_prox.deliver_later(wait_until: (@dias).days.from_now)
      end
    end
    def historialdeturnos
    end
    def hisotialdeturnospaciente
    end
    def aplicadasinturno
    end
    def searchturnos
       @fechainicio= params[:fechainicio]
         @fechafin= params[:fechafin]
         @covid1= params[:covid1]
         @covid2= params[:covid2]
        if params[:fechainicio].blank? or params[:fechafin].blank?
    	  redirect_to(historialdeturnos_path, alert: "Por favor ingrese una fecha de inicio y fin") and return  
       else 
         @fechainicio= params[:fechainicio]
         @fechafin= params[:fechafin]
         @covid1= params[:covid1]
         @covid2= params[:covid2]
         @fiebreamarilla= params[:fiebreamarilla]
         @gripe= params[:gripe]
         if 
             @turnos = Turno.all.where("fecha >= ? and fecha <= ? and user_id != ?", @fechainicio, @fechafin, "nil")
          end
        end
    end
end
