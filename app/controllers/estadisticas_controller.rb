class EstadisticasController < ApplicationController
  def mostrar
    @vacunados = 0
    @no_vac = 0
    @ausentes = 0
    @vac_covid = 0
    @vac_fiebre_am = 0
    @vac_gripe = 0
    @q = Turno.ransack(params[:q])
    @t = @q.result
    @t.each do |tur|
      if tur.asistencia == true
        if tur.aplicada == true
            @vacunados += 1
            if tur.nombrevacuna == "covid1" or tur.nombrevacuna == "covid2"
              @vac_covid += 1
            elsif tur.nombrevacuna == "fiebreamarilla"
              @vac_fiebre_am += 1
            else
              @vac_gripe += 1
            end
        else
          @no_vac += 1
        end
      else
        @ausentes += 1
      end
    end
  end
end
##A ver si con esto se arregla