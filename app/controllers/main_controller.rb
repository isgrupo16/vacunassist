class MainController < ApplicationController
  before_action :authenticate_user!
  def home
  end
 
  ##enfermeros
  def pantallaenfermero
  end
  def pantallaadmin
  end
  def pacientes
  end
  def search
    if params[:search].blank?  
    	redirect_to(pacientes_path, alert: "Por favor ingrese un DNI o Apellido") and return  
    else 
      @parameter = params[:search]
      if @parameter.first.match(/[0-9]/)
        @users = User.all.where("lower(DNI) LIKE :search", search: "%#{@parameter}%")
      else
          @parameter = params[:search].downcase
          @users = User.all.where("lower(apellido) LIKE :search", search: "%#{@parameter}%")
      end
    end
  end
  def searchenfermero
    if params[:search].blank?  
    	redirect_to(listadoenfermero_path, alert: "Por favor ingrese un DNI o Apellido") and return  
    else 
      @parameter = params[:search]
      if @parameter.first.match(/[0-9]/)
        @users = User.all.where("lower(DNI) LIKE :search", search: "%#{@parameter}%")
      else
          @parameter = params[:search].downcase
          @users = User.all.where("lower(apellido) LIKE :search", search: "%#{@parameter}%")
      end
    end
  end
  
  
  def ver
  end
  def verlistadoturnos
  end
  def verdetalleturno
  end
  def turnoscovid1
  end
  def turnoscovid2
  end
  def turnosfiebreamarilla
  end
  def turnosgripe
  end
  def obtenerturno
     @turno = Turno.find(params[:id])
     @turno.user_id = current_user.id
     @turno.save
     
     redirect_to root_path, notice: "Se obtuvo el turno exitosamente"
  end
  def cancelarturno
     @turno = Turno.find(params[:id])
     @turno.delete
     redirect_to verlistadoturnos_path, notice: "El turno fue cancelado exitosamente"
  end
    def cancelarturnocovid1
     @turno = Turno.find(params[:id])
     @turno.user_id = nil
     @turno.save
     redirect_to turnoscovid1_path(current_user), notice: "El turno fue cancelado exitosamente"
  end
  def cancelarturnocovid2
     @turno = Turno.find(params[:id])
     @turno.user_id = nil
     @turno.save
     redirect_to turnoscovid2_path(current_user), notice: "El turno fue cancelado exitosamente"
  end
  def alta
    e = User.find(params[:id])
    e.rol = 2
    e.fechaalta = Date.today
    e.save
    redirect_to "/pacientes"
  end
end
