class AddColumnToVacunatorios < ActiveRecord::Migration[6.1]
  def change
    add_column :vacunatorios, :email, :string
    add_column :vacunatorios, :telefono, :string
  end
end
