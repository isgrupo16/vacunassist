class AddTextoToTurnos < ActiveRecord::Migration[6.1]
  def change
    add_column :turnos, :texto, :string
  end
end
