class AddAsistenciaToTurnos < ActiveRecord::Migration[6.1]
  def change
    add_column :turnos, :asistencia, :boolean
  end
end
