class AddVacunatorioToComprobantes < ActiveRecord::Migration[6.1]
  def change
    add_reference :comprobantes, :vacunatorio, null: false, foreign_key: true
  end
end
