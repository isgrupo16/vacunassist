class CreateTurnos < ActiveRecord::Migration[6.1]
  def change
    create_table :turnos do |t|
      t.date :fecha
      t.time :hora
      t.string :nombrevacuna
      t.integer :idusuario

      t.timestamps
    end
  end
end
