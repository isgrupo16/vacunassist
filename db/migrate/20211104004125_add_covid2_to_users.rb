class AddCovid2ToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :covid2, :boolean
  end
end
