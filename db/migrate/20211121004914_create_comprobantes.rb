class CreateComprobantes < ActiveRecord::Migration[6.1]
  def change
    create_table :comprobantes do |t|
      t.string :vacuna
      t.date :fecha
      t.time :hora

      t.timestamps
    end
  end
end
