class AddHistoriaToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :historia, :attachment
  end
end
