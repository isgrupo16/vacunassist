class CreateVacunatorios < ActiveRecord::Migration[6.1]
  def change
    create_table :vacunatorios do |t|
      t.string :nombre
      t.string :lugar

      t.timestamps
    end
  end
end
