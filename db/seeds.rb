# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
u = User.new
u.password = "abc1234"
u.password_confirmation = "abc1234"
u.email = "test@gmail.com"
u.name = "Test"
u.apellido = "test1"
u.dni = 12345678
u.nacimiento = Date.today
u.direccion = "530 1234"
u.covid1 = true
u.covid2 = true
u.fiebreamarilla = false
u.gripe = false
u.rol = 1
u.save

u = User.new
u.password = "abc1234"
u.password_confirmation = "abc1234"
u.email = "dani-883345@hotmail.com"
u.name = "Adrián Daniel"
u.apellido = "Barral"
u.dni = 38917108
u.nacimiento = Date.today
u.direccion = "530 1234"
u.covid1 = true
u.covid2 = true
u.fiebreamarilla = false
u.gripe = false
u.rol = 3
u.save

t = Turno.new
t.fecha = Date.new(2021,12,4)
t.hora = Time.now
t.nombrevacuna = "gripe"
t.idusuario = 1
t.idvacunatorio = 1
t.vacunatorio_id = 1
t.user_id = 1
t.aplicada = false
t.asistencia = false
t.texto = ""
t.save

t = Turno.new
t.fecha = Date.new(2021,12,20)
t.hora = Time.now
t.nombrevacuna = "fiebreamarilla"
t.idusuario = 2
t.idvacunatorio = 1
t.vacunatorio_id = 1
t.user_id = 2
t.aplicada = false
t.asistencia = false
t.texto = ""
t.save

t = Turno.new
t.fecha = Date.new(2021,11,15)
t.hora = Time.now
t.nombrevacuna = "fiebreamarilla"
t.idusuario = 1
t.idvacunatorio = 1
t.vacunatorio_id = 1
t.user_id = 1
t.aplicada = false
t.asistencia = false
t.texto = ""
t.save